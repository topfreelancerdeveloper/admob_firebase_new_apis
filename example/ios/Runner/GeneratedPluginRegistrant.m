//
//  Generated file. Do not edit.
//

#import "GeneratedPluginRegistrant.h"

#if __has_include(<admob_firebase_new_apis/AdmobFirebaseNewApisPlugin.h>)
#import <admob_firebase_new_apis/AdmobFirebaseNewApisPlugin.h>
#else
@import admob_firebase_new_apis;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [AdmobFirebaseNewApisPlugin registerWithRegistrar:[registry registrarForPlugin:@"AdmobFirebaseNewApisPlugin"]];
}

@end
