package com.topfreelancerdeveloper.admob_firebase_new_apis

import android.app.Activity
import androidx.annotation.NonNull;
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.RequestConfiguration
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar

/** AdmobFirebaseNewApisPlugin */
public class AdmobFirebaseNewApisPlugin: FlutterPlugin, MethodCallHandler, ActivityAware {

  private lateinit var channel : MethodChannel
  private lateinit var activity: Activity
  private var ads = HashMap<String,RewardedAdWrapper>()

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "admob_firebase_new_apis")
    channel.setMethodCallHandler(this);
  }


 
  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar) {
      val channel = MethodChannel(registrar.messenger(), "admob_firebase_new_apis")
      val activity = registrar.activity()
      channel.setMethodCallHandler(AdmobFirebaseNewApisPlugin())
    }
  }


  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    if (call.method == "init") {
      var testDevices : List<*>? = (call.arguments as? HashMap<*, *>)?.get("testDevices") as? List<*>?

      MobileAds.initialize(activity , OnInitializationCompleteListener {
        initializationStatus -> result.success(true)
      })
      if(testDevices != null){
        MobileAds.setRequestConfiguration(RequestConfiguration.Builder().setTestDeviceIds(testDevices as? List<String>).build())
      }
      return
    }
    if (call.method == "create") {

      var adUnitId : String? = (call.arguments as? HashMap<*, *>)?.get("adUnitId") as? String?
      var forceCreate: Boolean? = (call.arguments as? HashMap<*, *>)?.get("forceCreate") as? Boolean?
      if (adUnitId == null || adUnitId?.length == 0){

        result.error("-1" , "empty or null adunitid" , null)
        return
      }
      if (forceCreate == null || forceCreate == false) {
        if (ads.contains(adUnitId)){
          result.error("-2" , "ad unit already has created", null)
        return
      }
      }
      ads[adUnitId] = RewardedAdWrapper(activity,adUnitId,channel)
      result.success(true)
      return
    }
    if (call.method == "load") {
      var adUnitId : String? = (call.arguments as? HashMap<*, *>)?.get("adUnitId") as? String?
      if (adUnitId == null || adUnitId?.length == 0){

        result.error("-1" , "empty or null adunitid" , null)
        return
      }
      if(ads[adUnitId] == null) {
        result.error("-2" , "ad unit id has no wrapper" , null)
        return
      }
      val cb  = object : RewardedAdLoadCallback(){
        override fun onRewardedAdLoaded() {
          result.success(true)
        }
        override fun onRewardedAdFailedToLoad(errorCode: Int) {
          result.error(errorCode.toString() , "ad load failed" , "failed ad load")
        }
      }
      ads[adUnitId]?.load(cb)
      return
    }
    if (call.method == "show") {
      var adUnitId : String? = (call.arguments as? HashMap<*, *>)?.get("adUnitId") as? String?
      var userId : String? = (call.arguments as? HashMap<*, *>)?.get("userId") as? String?
      var customData : String? = (call.arguments as? HashMap<*, *>)?.get("customData") as? String?
      if (adUnitId == null || adUnitId?.length == 0){

        result.error("-1" , "empty or null adunitid" , null)
        return
      }
      if(ads[adUnitId] == null) {
        result.error("-2", "ad unit id has no wrapper", null)
        return
      }
      if(ads[adUnitId]?.isLoaded() == false){
        result.error("-3", "ad not loaded", null)
        return
      }
      ads[adUnitId]?.show(userId , customData)
      result.success(true)
      return
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

  override fun onDetachedFromActivity() {

  }

  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {

  }

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    activity = binding.activity
  }

  override fun onDetachedFromActivityForConfigChanges() {

  }
}
