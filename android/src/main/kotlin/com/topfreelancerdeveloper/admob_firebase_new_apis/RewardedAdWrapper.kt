package com.topfreelancerdeveloper.admob_firebase_new_apis

import android.app.Activity
import android.util.Log
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.rewarded.*
import io.flutter.plugin.common.MethodChannel
import java.lang.Exception

class RewardedAdWrapper(context : Activity , adUnitId : String, channel: MethodChannel) : RewardedAdCallback() {

    private var channel : MethodChannel = channel
    private var context : Activity = context
    private var adUnitId : String = adUnitId
    private var rewardedAd: RewardedAd = RewardedAd(context , adUnitId)

    fun show(userId : String?, customData : String?){
        if(userId != null || customData != null) {
            try {
                rewardedAd.setServerSideVerificationOptions(
                        ServerSideVerificationOptions.Builder().setCustomData(customData)
                                .setUserId(userId)
                                .build()
                );
            }catch (err : Exception){
                Log.e("admob firebase new apis" , err.toString())
            }
        }
        rewardedAd.show(context , this);
    }

    fun load(rewardedAdLoadCallback: RewardedAdLoadCallback){
        rewardedAd.loadAd(AdRequest.Builder().build() , rewardedAdLoadCallback)
    }
    fun isLoaded() : Boolean {
        return rewardedAd.isLoaded()
    }
    override fun onRewardedAdFailedToShow(p0: Int) {
        var args  = HashMap<String,Any>()
        args["adUnitId"] = adUnitId
        args["error"] = p0.toString()
        channel?.invokeMethod("PlaybackFailure" , args)
    }

    override fun onRewardedAdClosed() {
        var args =  HashMap<String,Any>()
        args["adUnitId"] = adUnitId
        channel?.invokeMethod("Closed",  args)
    }

    override fun onRewardedAdOpened() {
        var args =  HashMap<String,Any>()
        args["adUnitId"] = adUnitId
        channel?.invokeMethod("Opened",  args)
    }

    override fun onUserEarnedReward(p0: RewardItem) {
        var args =  HashMap<String,Any>()
        args["adUnitId"] = adUnitId
        args["type"] = p0.type
        args["amount"] = p0.amount
        channel?.invokeMethod("Rewarded",  args)
    }

}