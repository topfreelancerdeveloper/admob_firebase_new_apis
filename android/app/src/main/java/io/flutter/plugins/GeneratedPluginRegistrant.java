package io.flutter.plugins;

import io.flutter.plugin.common.PluginRegistry;
import com.topfreelancerdeveloper.admob_firebase_new_apis.AdmobFirebaseNewApisPlugin;

/**
 * Generated file. Do not edit.
 */
public final class GeneratedPluginRegistrant {
  public static void registerWith(PluginRegistry registry) {
    if (alreadyRegisteredWith(registry)) {
      return;
    }
    AdmobFirebaseNewApisPlugin.registerWith(registry.registrarFor("com.topfreelancerdeveloper.admob_firebase_new_apis.AdmobFirebaseNewApisPlugin"));
  }

  private static boolean alreadyRegisteredWith(PluginRegistry registry) {
    final String key = GeneratedPluginRegistrant.class.getCanonicalName();
    if (registry.hasPlugin(key)) {
      return true;
    }
    registry.registrarFor(key);
    return false;
  }
}
