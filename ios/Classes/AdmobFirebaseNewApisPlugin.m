#import "AdmobFirebaseNewApisPlugin.h"
#if __has_include(<admob_firebase_new_apis/admob_firebase_new_apis-Swift.h>)
#import <admob_firebase_new_apis/admob_firebase_new_apis-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "admob_firebase_new_apis-Swift.h"
#endif

@implementation AdmobFirebaseNewApisPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftAdmobFirebaseNewApisPlugin registerWithRegistrar:registrar];
}
@end
