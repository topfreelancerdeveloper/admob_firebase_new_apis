

import Foundation
import GoogleMobileAds

class GADRewardedWrapper :GADRewardedAd, GADRewardedAdDelegate{
    
    var channel : FlutterMethodChannel?
    
    init(adUnitID id : String , channel ch : FlutterMethodChannel) {
        super.init(adUnitID: id)
        channel = ch
    }
    
    /// Tells the delegate that the user earned a reward.
    func rewardedAd(_ rewardedAd: GADRewardedAd, userDidEarn reward: GADAdReward) {
        var args =  [String: Any]()
        args["adUnitId"] = adUnitID
        args["type"] = reward.type
        args["amount"] = reward.amount
        channel?.invokeMethod("Rewarded", arguments: args)
    }
    /// Tells the delegate that the rewarded ad was presented.
    func rewardedAdDidPresent(_ rewardedAd: GADRewardedAd) {
      var args =  [String: Any]()
      args["adUnitId"] = adUnitID
      channel?.invokeMethod("Opened", arguments: args)
    }
    /// Tells the delegate that the rewarded ad was dismissed.
    func rewardedAdDidDismiss(_ rewardedAd: GADRewardedAd) {
      var args =  [String: Any]()
      args["adUnitId"] = adUnitID
      channel?.invokeMethod("Closed", arguments: args)
    }
    /// Tells the delegate that the rewarded ad failed to present.
    func rewardedAd(_ rewardedAd: GADRewardedAd, didFailToPresentWithError error: Error) {
      var args =  [String: Any]()
      args["adUnitId"] = adUnitID
      args["error"] = error.localizedDescription
      channel?.invokeMethod("PlaybackFailure", arguments: args)
    }
}
