import Flutter
import UIKit
import GoogleMobileAds

public class SwiftAdmobFirebaseNewApisPlugin: NSObject, FlutterPlugin {
    var ads = [String : GADRewardedWrapper]()
    private static var channel = FlutterMethodChannel()
    
  public static func register(with registrar: FlutterPluginRegistrar) {
    channel = FlutterMethodChannel(name: "admob_firebase_new_apis", binaryMessenger: registrar.messenger())
    let instance = SwiftAdmobFirebaseNewApisPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    if call.method == "init" {
        let testDevices: [String]? = (call.arguments as? [String: Any])?["testDevices"] as? [String]
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        if testDevices != nil {
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = testDevices
        }
        result(true)
        return
    }
    if call.method == "create" {
        let adUnitId: String? = (call.arguments as? [String: Any])?["adUnitId"] as? String
        let forceCreate: Bool? = (call.arguments as? [String: Any])?["forceCreate"] as?Bool
        if adUnitId == nil || adUnitId?.count == 0{
            result(FlutterError(code: "-1",
                message: "empty or null adunitid",
                details: nil)
            )
            return
        }
        if forceCreate == nil || forceCreate == false {
        if ads.contains(where: { (key: String, value: GADRewardedWrapper) -> Bool in
            key == adUnitId
        }){
           result(FlutterError(code: "-2",
                message: "ad unit already has created",
                details: nil)
            )
            return
        }
        }
        ads[adUnitId ?? ""] = GADRewardedWrapper(adUnitID: adUnitId ?? "l", channel: SwiftAdmobFirebaseNewApisPlugin.channel)
        result(true)
        return
    }
    if call.method == "load" {
        let adUnitId: String? = (call.arguments as? [String: Any])?["adUnitId"] as? String
        if adUnitId == nil || adUnitId?.count == 0{
            result(FlutterError(code: "-1",
                message: "empty or null adunitid",
                details: nil)
            )
            return
        }
        
        if ads[adUnitId ?? ""] == nil {
            result(FlutterError(code: "-2",
                message: "ad unit id has no wrapper",
                details: nil)
            )
            return
        }
        
        ads[adUnitId ?? ""]?.load(GADRequest()){ error in
          if let error = error {
            result(FlutterError(code: error.code.description,
            message: "load failed",
            details: error.localizedDescription))
          } else {
            result(true)
          }
        }
        return
    }
    if call.method == "show" {
        let adUnitId: String? = (call.arguments as? [String: Any])?["adUnitId"] as? String
	let userId: String? = (call.arguments as? [String: Any])?["userId"] as? String
	let customData: String? = (call.arguments as? [String: Any])?["customData"] as? String
        if adUnitId == nil || adUnitId?.count == 0{
            result(FlutterError(code: "-1",
                message: "empty or null adunitid",
                details: nil)
            )
            return
        }
        
        if ads[adUnitId ?? ""] == nil {
            result(FlutterError(code: "-2",
                message: "ad unit id has no wrapper",
                details: nil)
            )
            return
        }
	var ssvOptions = GADServerSideVerificationOptions()
	ssvOptions.userIdentifier = userId
	ssvOptions.customRewardString = customData
	ads[adUnitId ?? ""]?.serverSideVerificationOptions = ssvOptions
        
        if ads[adUnitId ?? ""]?.isReady == false{
            result(FlutterError(code: "-3",
                message: "not loaded",
                details: nil)
            )
            return
        }
        ads[adUnitId ?? ""]?.present(fromRootViewController: (UIApplication.shared.delegate?.window??.rootViewController)!, delegate: ads[adUnitId ?? ""]!)
        result(true)
        return
    }
    result("iOS " + UIDevice.current.systemVersion)
  }
}
